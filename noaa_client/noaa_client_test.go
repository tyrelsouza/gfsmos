package noaa_client

import (
	"github.com/stretchr/testify/assert"
	"gopkg.in/h2non/gock.v1"
	"io/ioutil"
	"log"
	"testing"
)
func fakeResponse() string {
	b, err := ioutil.ReadFile("../test/KeeneResponse.html") // b has type []byte
	if err != nil {
		log.Fatal(err)
	}
	return string(b)
}


func TestFoo(t *testing.T) {
	defer gock.Off() // Flush pending mocks after test execution

	gock.New("https://www.nws.noaa.gov").
		Get("/cgi-bin/mos/getmav.pl").
		MatchParam("sta", "KEEN").
		Reply(200).
		BodyString(fakeResponse())

		// Use Client & URL from our local test server
	api := API{
		"KEEN",
	}

	body := api.GetDataRaw()

	assert.Equal(t, "bob", body)
}