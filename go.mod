module gitlab/tyrelsouza/gfsmos

go 1.13

require (
	github.com/stretchr/testify v1.5.1
	github.com/urfave/cli/v2 v2.1.1
	gopkg.in/h2non/gock.v1 v1.0.15
)
